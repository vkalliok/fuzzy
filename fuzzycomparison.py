import urllib.request
import urllib.error
import socket
import sys
from sys import stdout
import ssdeep
import edlib
import json
from json import JSONDecodeError
from datetime import datetime
import os


class Config:
    def __init__(self, 
                 use_stdout=False,
                 verbose_printing=True,
                 comparison_value_threshold=90,
                 edit_threshold=0.1,
                 timeout=10):

        self.use_stdout = use_stdout
        self.verbose_printing = verbose_printing
        self.comparison_value_threshold = comparison_value_threshold
        self.edit_threshold = edit_threshold
        self.timeout = timeout


def download_page(url, timeout):
    try:
        headers = {
                   'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.3'
                   }
        req = urllib.request.Request(url=url, headers=headers)
        site = urllib.request.urlopen(req, timeout=timeout)
        return site, site.getcode()

    except urllib.error.HTTPError as e:
        return None, e.code

    except urllib.error.URLError as e:
        if hasattr(e, 'reason'):
            return None, e.reason

        if hasattr(e, 'code'):
            return None, e.code

    except ConnectionResetError as e:
        return None, e

    except socket.timeout:
        return None, 'timed out'
        return None, None


def main():
    config = Config()

    if len(sys.argv) < 3:
        print('Usage: {} <json file> <output path>'.format(sys.argv[0]))
        sys.exit(0)
    else:
        input_file = sys.argv[1]
        output_path = sys.argv[2]

    verboseprint = print if config.verbose_printing else lambda *a, **k: None

    try:
        with open(input_file, 'r') as f:

            webpages = json.load(f)
    except JSONDecodeError:
        verboseprint('Malformed JSON file')
        sys.exit(1)

    changed_pages = []

    for webpage in webpages:

        verboseprint('Fetching {}...'.format(webpage['url']))
        start = datetime.now()
        contents, error = download_page(webpage['url'], config.timeout)
        timestamp = datetime.now()
        time_spent = (timestamp - start).total_seconds()

        if contents is not None:
            new_version = contents.read()
            hash_new = ssdeep.hash(new_version)
            verboseprint("Result: {}\n Time spent: {}".format(contents.code, time_spent))

            if ('hash' in webpage and
                    webpage['hash'] is not None and
                    webpage['hash'] != 'Could not retrieve webpage'):

                hash_old = webpage['hash']
                webpage['hash-old'] = hash_old
                webpage['comparison value'] = ssdeep.compare(hash_old, hash_new)

                verboseprint('Old hash: {}'.format(hash_old))
                verboseprint('New hash: {}'.format(hash_new))
                verboseprint('Comparison value: {}'.format(webpage['comparison value']))

            if 'timestamp' in webpage:
                webpage['timestamp-old'] = webpage['timestamp']
                webpage['human-readable timestamp-old'] = webpage['human-readable timestamp']

            webpage['hash'] = hash_new

            webpage['timestamp'] = timestamp.timestamp()
            webpage['human-readable timestamp'] = timestamp.isoformat()

            if 'previous version' in webpage:
                verboseprint('Calculating Levenshtein edit distance...')

                edit_distance = edlib.align(new_version, webpage['previous version'])['editDistance']
                webpage['edit distance'] = edit_distance
                webpage['relative edit distance'] = edit_distance / len(webpage['previous version'])
                verboseprint('edit distance: {} - relative distance: {}'.format(edit_distance, webpage['relative edit distance']))
            else:
                'No previous version, skipping Levenshtein edit distance...'
            webpage['previous version'] = str(new_version)

            if ('comparison value' in webpage and
                    webpage['comparison value'] < config.comparison_value_threshold or 
                    'relative edit distance' in webpage and
                    webpage['relative edit distance'] > config.edit_threshold):
                changes = {
                           'url': webpage['url'],
                           'comparison value': webpage['comparison value'],
                           'relative edit distance': webpage['relative edit distance']
                          }
                changed_pages.append(changes)


        else:
            if error is not None:
                webpage['error'] = str(error)
                verboseprint('ERROR: {}\n Time spent: {}'.format(error, time_spent))

            webpage['hash'] = 'Could not retrieve webpage'
            changes = {
                       'url': webpage['url'],
                       'error': error
                      }

    if not config.use_stdout:
        output_file = os.path.join(output_path, os.path.basename(input_file))
        with open(output_file, 'w') as f:
            f.write(json.dumps(webpages, indent=2, sort_keys=True))

        if len(changed_pages) > 0:
            original_file = os.path.splitext(os.path.basename(input_file))[0]
            output_file = os.path.join(output_path, original_file + '_changes.json')
            with open(output_file, 'w') as f:
                f.write(json.dumps(changed_pages, indent=2, sort_keys=True))
    else:
        stdout.write(json.dumps(webpages, sort_keys=True))
        stdout.flush()


if __name__ == "__main__":
    main()
