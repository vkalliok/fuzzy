#!/bin/sh

input_path="resource-webpages"
output_path="resource-webpages-out"
set -e
mkdir -p /home/appuser/
git clone resource-webpages resource-webpages-out

git config --global user.email "nobody@cincan.io"
git config --global user.name "Concourse"
ls

for filename in $input_path/*.json
do
    python3 $input_path/fuzzycomparison.py $filename $output_path

done

cd resource-webpages-out
git add *
git commit -m "Scanned and compared webpages"
